<?php
require('fpdf182/fpdf.php');

class Pdf_generator extends FPDF
{
    public $header_params=[];
    public $pdf_type;
    public $to_params=[];
    public $obj_params=[];
    public $date_params;
    public $date_facture;
    public $tableau=[];
    public $params_poste=[];
    public $footer_param;

    public function __construct($header_params, $pdf_type, $to_params, $obj_params, $date_params, $date_facture, $tableau, $params_poste, $footer_param, $orientation = 'P', $unit = 'mm', $size = 'A4')
    {
        parent::__construct($orientation, $unit, $size);
        $this->header_params = $header_params;
        $this->pdf_type = $pdf_type;
        $this->to_params = $to_params;
        $this->obj_params = $obj_params;
        $this->date_params = $date_params;
        $this->date_facture = $date_facture;
        $this->tableau = $tableau;
        $this->params_poste = $params_poste;
        $this->footer_param = $footer_param;
    }

    public function Header()
    {
        if ($this->pdf_type == 3 || $this->pdf_type == 2){
            // Logo
            $this->Image($this->header_params[0], 15, 15, 75);
            $this->SetLeftMargin(15);
            $this->SetXY(15, 50);
            for ($i=1, $iMax = count($this->header_params) -1; $i< $iMax; $i++){
                // Police Arial gras 15
                $this->SetFont('Arial', '', 8);
                $w = $this->GetStringWidth($this->header_params[$i][0] .' : ');
                $this->Cell($w, 5, $this->header_params[$i][0] .' : ', 0, 0);
                $this->SetFont('Arial', 'B', 8);
                $this->Cell(45, 5, $this->header_params[$i][1], 0, 1);
            }

            $this->SetFont('Arial', '', 8);
            $w = $this->GetStringWidth($this->header_params[count($this->header_params)-1]);
            $this->Cell($w, 5, iconv("UTF-8", "CP1252",$this->header_params[count($this->header_params)-1]), 0, 1);
        }

    }
    public function Footer()
    {
        if ($this->pdf_type == 3 || $this->pdf_type == 2){
            // Positionnement à 1,5 cm du bas
            $this->SetY(-15);
            // Arial italique 8
            $this->SetFont('Arial','',8);
            // Numéro de page
            $this->Cell(0,10,$this->footer_param,0,0,'C');
        }
    }

    public function Write_to()
    {
        if ($this->pdf_type == 3 || $this->pdf_type == 2){
            $this->SetFont('Arial', '', 10);
            for ($i=1, $iMax = count($this->header_params); $i< $iMax; $i++){
                $this->Cell(0, 5,  iconv("UTF-8", "CP1252", $this->to_params[$i]), 0, 1, 'R');
            }
        }
    }
    public function Write_obj()
    {
        if ($this->pdf_type == 3 || $this->pdf_type == 2){
            $this->SetFont('Arial', '', 10);
            for ($i=1, $iMax = count($this->header_params); $i< $iMax; $i++){
                $this->Cell(0, 5,  iconv("UTF-8", "CP1252",$this->obj_params[$i]), 0, 1);
            }
        }
    }

    public function Write_date()
    {
        $this->Cell(0, 5,  iconv("UTF-8", "CP1252",$this->date_params), 0, 1, 'R');
        $this->Ln(5);
    }
    public function Write_date_facture()
    {
        if ($this->pdf_type == 3){
            $this->SetFont('Arial', 'B', 16);
            $this->Cell(0, 5,  iconv("UTF-8", "CP1252",'DEVIS ' . $this->date_facture), 0, 1, 'C');
        } elseif ($this->pdf_type == 2){
            $this->SetFont('Arial', 'B', 16);
            $this->Cell(0, 5,  iconv("UTF-8", "CP1252",'DEVIS N' . $this->date_facture), 0, 1, 'C');
        }
        $this->Ln(5);
    }
    public function addTH($header)
    {
        if ($this->pdf_type == 3 || $this->pdf_type == 2) {
            // Largeurs des colonnes
            $w = array(80, 35, 35, 35);
            for ($i = 0, $iMax = count($header); $i < $iMax; $i++)
                $this->Cell($w[$i], 7, iconv("UTF-8", "CP1252", $header[$i]), 1, 0, $this->getalign($i));
            $this->Ln();
        }
    }
    public function addRow($row)
    {
        if ($this->pdf_type == 3 || $this->pdf_type == 2) {
            // Largeurs des colonnes
            $w = array(80, 35, 35, 35);
            foreach ($row as $key => $ligne) {
                for ($i = 0, $iMax = count($ligne); $i < $iMax; $i++) {
                    if ($key == count($row) - 1) {
                        $this->Cell($w[$i], 7, iconv("UTF-8", "CP1252", $ligne[$i]), 'LRB', 0, $this->getalign($i), false);
                    } else {
                        $this->Cell($w[$i], 7, iconv("UTF-8", "CP1252", $ligne[$i]), 'LR', 0, $this->getalign($i), false);
                    }

                }
                $this->Ln();
            }
        }
    }
    public function addTH2($header)
    {
        // Largeurs des colonnes
        $w = array(80, 30);
        for($i=0, $iMax = count($header); $i< $iMax; $i++)
            if($i==0){
                $this->Cell($w[$i],7,iconv("UTF-8", "CP1252",$header[$i]),'LBT',0,$this->getalign($i));
            } else{
                $this->Cell($w[$i],7,iconv("UTF-8", "CP1252",$header[$i]),'RBT',0,$this->getalign($i));
            }
        $this->Ln();
    }

    function getalign($i){
        if ($i==0){
            return 'L';
        } else{
            return 'C';
        }
    }

    public function addRow2($row)
    {
        // Largeurs des colonnes
        $w = array(80, 30);
        foreach ($row as $key=>$ligne){
            for($i=0, $iMax = count($ligne); $i< $iMax; $i++) {
                if($key==count($row)-1){
                    $this->Cell($w[$i], 7, iconv("UTF-8", "CP1252",$ligne[$i]), 'LRB', 0, $this->getalign($i), false);
                } else{
                    $this->Cell($w[$i], 7, iconv("UTF-8", "CP1252",$ligne[$i]), 'LR', 0, $this->getalign($i), false);
                }

            }
            $this->Ln();
        }
    }

    public function BuildTable()
    {
        $this->SetFont('Arial', '', 11);
        $this->addTH(['Service', 'Nombre', 'Prix par unité', 'Total']);
        for ($i=0, $iMax = count($this->tableau)-1; $i< $iMax; $i++){
            $this->addRow($this->tableau[$i]);
        }
        $this->SetFont('Arial', 'B', 11);
        $this->addRow([$this->tableau[count($this->tableau)-1]]);
    }

    public function BuildReglement(){
        $this->SetFont('Arial', '', 11);
        $this->Ln(10);

        $this->addTH2(['Mode de règlement', '']);

        $this->addRow2([['Par Chèque bancaire ou virement à réception', '']]);

        $this->Ln(5);

        $this->Cell(0, 5,  iconv("UTF-8", "CP1252", 'TVA non applicable, article 293 B du code général des impôts'), 0, 1, 'L');

        $this->Ln(5);
    }

    public function MarkPoste()
    {
        foreach ($this->params_poste as $par){
            $this->Cell(0, 5,  iconv("UTF-8", "CP1252", $par), 0, 1, 'R');
        }
    }

    public function Build(){
        if ($this->pdf_type == 3 || $this->pdf_type == 2){
            $this->AddPage();
            $this->Write_to();
            $this->Write_obj();
            $this->Write_date();
            $this->Write_date_facture();
            $this->BuildTable();
            $this->BuildReglement();
            $this->MarkPoste();
            $this->Output();
        }
    }
}