<?php
require('../code/Pdf_generator.php');

$pdf_header = ['../code/cropped-Logo-RLG-2014-Ecriture-noire.jpg', ['PRESIDENT', 'Dr Jean Marc CHAPPLAIN'], ['DIRECTEUR', 'Patrice PRETER'], ['COORDINATEUR MEDICAL', 'Dr Didier MICHEL'],'Tél: : 02 99 32.47 36/ Fax : 02.99.50.51.20'];
$pdf_type = 3;
$to_params = ['A l’attention de M Guelfucci', 'IME La Clarté', '28bis, rue Saint Michel', '35600 Redon'];
$obj_params = ['e-mail :interpretariat@rlg35.org', 'Nos réf. : 19-041', 'Objet : intervention interprète', 'N° : de Siret : 402 810 295 000 53'];
$date_params = 'A Rennes, le 4 mars 2019';
$date_facture = '06/2019';
$tableau = [
    [
        ['Interprétariat en turc le 19/03/2019', '1 heure', '38€', '38€'],
        ['Temps de déplacement aller-retour', '2 heures','38€','76€'],
        ['Frais de déplacement (réf. Mappy)','66*2=54km','0,54€','71,28€'],
    ],
    [
        ['Interprétariat en turc le 04/06/2019', '1 heure', '38€', '38€'],
        ['Temps de déplacement aller-retour', '2 heures','38€','76€'],
        ['Frais de déplacement (réf. Mappy)','66*2=54km','0,54€','71,28€'],
    ],
    [
        ['', '', 'Net à payer :', '370,56€']
    ],
];
$params_poste = ['Mme Oksana VATS', 'Responsable du pôle interprétariat'];

$footer_param = ['Réseau Louis Guilloux - Association loi 1901 – 12ter avenue de Pologne 35200 RENNES'];

$pdf = new Pdf_generator($pdf_header, $pdf_type, $to_params, $obj_params, $date_params, $date_facture, $tableau, $params_poste, $footer_param);
$pdf->Build();